<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1> Buat Account Baru </h1>
    <h3> <b> Form Sign Up </b> </h3>
    
    <form action="/welcome" method="post">
    @csrf
        <ul>
            <label> First Name : </label> <br><br>
            <input type="text" name="fname" id="fname"> <br><br>
        </ul>
        <ul>
            <label> Last Name : </label> <br><br>
            <input type="text" name="lname" id="lname"> <br><br>
        </ul>
        <ul>
            <label> Gender : </label> <br><br>
            <input type="radio" name="gender" id="gmale"> Male <br>
            <input type="radio" name="gender" id="gfemale"> Female <br>
            <input type="radio" name="gender" id="gother"> Other <br>
        </ul>
        <ul>
            <label> Nationality : </label> <br><br>
            <select name="nation" id="nation">
                <option value=""> -- Pilih Nationality -- </option>
                <option value=""> Indonesia </option>
                <option value=""> Belanda </option>
                <option value=""> Inggris </option>
            </select> <br><br>
        </ul>
        <ul>
            <label> Language Spoken : </label> <br><br>
            <input type="checkbox" name="bhs" id="idn"> Indonesia <br><br>
            <input type="checkbox" name="bhs" id="eng"> Inggris <br><br>
            <input type="checkbox" name="bhs" id="ned"> Belanda <br><br>
            <input type="checkbox" name="bhs" id="oth"> Other <br><br>
        </ul>
        <ul>
            <label> Bio : </label> <br><br>
            <textarea name="bio" id="bio" cols="36" rows="17"></textarea> <br><br>
        </ul>
        <input type="submit" name="submit" value="kirim"> 
    </form>

</body>
</html>
