<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registrasi() {
        return view('mediahome.registrasi');
    }
    public function welcome(Request $request) {
        $fname = $request->fname;
        $lname = $request->lname;
        
        return view('mediahome.welcome', compact('fname', 'lname'));
    }
}
